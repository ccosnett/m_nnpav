BeginPackage["nnpav`"];
Unprotect["nnpav`*"]; ClearAll["nnpav`*"]; ClearAll["nnpav`Private`*"];


m2TargetParams::usage = "m2TargetParams[aid]";
nHRTargetParams::usage = "nHRTargetParams[aid]";
queryNHR::usage = "queryNHR[expr]";



(*####################*)(*####################*)(*####################
DATABASE = "avm";
Print["database = " <> DATABASE];
Needs["DatabaseLink`"];
conn = OpenSQLConnection[JDBC["MySQL(Connector/J)", "localhost:3306/"<>DATABASE<>"?useSSL=false"], "Username" -> "student", "Password" -> "1.2323951EsC"];
database = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", "Port" -> 3306, "Name" -> DATABASE, "Username" -> "student", "Password" -> "1.2323951EsC"|>];
####################*)(*####################*)(*####################*)


Begin["`Private`"];

(*####################*)(*####################*)(*####################*)
DATABASE = "nhr";
Print["database = " <> DATABASE];
Needs["DatabaseLink`"];
conn = OpenSQLConnection[JDBC["MySQL(Connector/J)", "localhost:3306/"<>DATABASE<>"?useSSL=false"], "Username" -> "alice", "Password" -> "MyStrongPass."];
database = DatabaseReference[<|"Backend" -> "MySQL", "Host" -> "localhost", "Port" -> 3306, "Name" -> DATABASE, "Username" -> "alice", "Password" -> "MyStrongPass."|>];
(*####################*)(*####################*)(*####################*)

queryNHR[expr_] := Module[{},
    out1 = SQLExecute[conn
                 , expr
                 , "GetAsStrings" -> False];
    out1
];



m2TargetParams[aid_]  := Module[{df, columns, vector,cols},
    vector = query["SELECT
		ranked_number,
        postcode,
        thoroughfare,
        TYPE1,
        TYPE2,
        TYPE3,
        TYPE4
	FROM
		(
		 SELECT
			ranked_number,
            postcode,
            thoroughfare,
            NP_PD_SUMM0_TO_9_type1_PC AS TYPE1,
            NP_PD_SUMM0_TO_9_type2_PC AS TYPE2,
            NP_PD_SUMM0_TO_8_MAX_type3_PC AS TYPE3,
			NP_PD_SUMM0_TO_9_type4_PC AS TYPE4
		FROM
			liqquid_addresshub ah
		LEFT JOIN
			liqquid_epc_certs epc ON ah.aid = epc.aid
		LEFT JOIN
			nn_type nnpav ON ah.aid = nnpav.aid
		WHERE
			ah.aid = "<>aid<>"
		) s;"]//Flatten;
  columns = {
       "targetRankedNum"   (*ranked_num*)
      ,"targetPostcode"    (*pc*)
      ,"targetThoroughfare"(*street*)
      ,"targetType1"       (*p_type1*)
      ,"targetType2"       (*p_type2*)
      ,"targetType3"      (*p_type3*)
      ,"targetType4"      (*p_type4*)
       };
    cols = {
        "ranked_num",
        "pc",
        "street",
        "p_type1",
        "p_type2",
        "p_type3",
        "p_type4"
    };

  df = Dataset[AssociationThread[Rule[columns,vector]]];
  df
];

nHRTargetParams[aid_]  := Module[{df, columns, vector,cols,q},

        (* grab info on the target property including ranked number, postcode, thoroughfare, property type category, and Type 1, 2, 3, and 4, code *)
        (* these are stored in user defined variables declared above *)

q="SELECT
		ranked_number,
        postcode,
        thoroughfare,
        TYPE1,
        TYPE2,
        TYPE3,
        TYPE4,
        latitude4326,
	    longitude4326
	FROM
		(
		 SELECT
			ranked_number,
            postcode,
            thoroughfare,
            propTypeCategory,
            CASE WHEN propTypeCategory IS NULL AND propertyType IS NULL THEN nnpav.NP_PD_SUMM0_TO_9_type4_PC ELSE propertyType END AS pType,
               latitude4326,
		        longitude4326
		FROM
			liqquid_addresshub ah
		LEFT JOIN
			liqquid_epc_certs epc ON ah.aid = epc.aid
		LEFT JOIN
			nn_type nnpav ON ah.aid = nnpav.aid
		WHERE
			ah.aid = " <> ToString[aid]<>"
		) s
	LEFT JOIN
		avm_property_type_matrix ptm ON s.pType = ptm.Type4;";
CopyToClipboard[q];
Print["COPIED"];

    vector = queryNHR[q]//Flatten;

  columns = {
       "targetRankedNum"   (*ranked_num*)
      ,"targetPostcode"    (*ranked_num*)
      ,"targetThoroughfare"(*ranked_num*)
      ,"targetType1"       (*ranked_num*)
      ,"targetType2"       (*ranked_num*)
      ,"targetType3"      (*p_type3*)
      ,"targetType4"      (*p_type4*)
      ,"target_lat"
      ,"target_lon"
       };
    cols = {
        "ranked_num",
        "pc",
        "street",
        "p_type1",
        "p_type2",
        "p_type3",
        "p_type4"
        ,"target_lat"
      ,"target_lon"
    };

  df = Dataset[AssociationThread[Rule[columns,vector]]];
  df
];

nHRcreateNearestPostcodesTable[nearestPostcodesList_] := Module[{name},
  name = "nearest_postcodes";
  SQLDropTable[conn, name];
  SQLCreateTable[conn, name, {SQLColumn["postcode", "DataTypeName" -> "VARCHAR(10)"]}];
  SQLInsert[conn, name, {"postcode"}, {#} & /@ nearestPostcodesList];
  SQLSelect[conn, "nearest_postcodes"]
];
nHRHousesInPostcode[aid_]:=Module[{df, targetPostcode, out, cols},
    df = nHRTargetParams[aid];
    targetPostcode = df["targetPostcode"];

    query["SET @row_number = 0;"];
    out = query["SELECT
            (@row_number:=@row_number + 1) AS row_num,
            aid,
            TYPE1,
            TYPE3,
            TOTAL_FLOOR_AREA,
            NUMBER_HABITABLE_ROOMS
		FROM
			(
			 SELECT
				aid,
                TYPE1,
                TYPE3,
                TOTAL_FLOOR_AREA,
                NUMBER_HABITABLE_ROOMS
			FROM
				(
                 SELECT
					buildingNumber,
                    ah.aid,
                    ranked_number,
                    sub_ranked_number,
                    postcode,
					CASE WHEN propertyType IS NULL THEN nnpav.NP_PD_SUMM0_TO_9_type4_PC ELSE propertyType END AS pType,
                    TOTAL_FLOOR_AREA,
                    NUMBER_HABITABLE_ROOMS
				FROM liqquid_addresshub ah
				LEFT JOIN
					liqquid_epc_certs epc ON ah.aid = epc.aid
				LEFT JOIN
					nn_type nnpav ON ah.aid = nnpav.aid WHERE postcode='"<>targetPostcode<>"'
				) s
			LEFT JOIN
				avm_property_type_matrix ptm ON s.pType = ptm.Type4
            ORDER BY
				ranked_number, sub_ranked_number
			) t;"];
    cols= {"row_num", "aid", "TYPE1", "TYPE3", "total_floor_area", "number_habitable_rooms"};
    Dataset@Map[AssociationThread[cols, #] &]@out

];
nHRTargetRowNum[aid_] := Module[{df, out},
   df = nHRHousesInPostcode[aid];
   out = df[Select[#"aid" == aid &], "row_num"];
   First@Normal[out ]
   ];
nHRNumRows[aid_] := Module[{df, out},
   df = nHRHousesInPostcode[aid];
   out = Length[df ]];
nHRMeanM2Type3PC[aid_] := Module[{df, targetPostcode, targetType3, out, out2},
   df = nHRTargetParams[aid];
   targetPostcode = df["targetPostcode"];
   targetType3 = df["targetType3"];
   out = query[
     "SELECT mean_EPC_M2_type3_PC FROM avm_mean_EPC_type3_PC WHERE \
postcode = '" <> targetPostcode <> "' AND propertyType = '" <>
      targetType3 <> "';"];
   out2 = Flatten[out];
   First[out2]
   ];

nHRAntecedent1aSubquery1[aid_] := Module[{df, targetRowNumber, out, interestingIntermediateResult},
    targetRowNumber = nHRTargetRowNum[aid];
    df = nHRHousesInPostcode[aid];
    interestingIntermediateResult = df[Select[(Abs[#"row_num" - targetRowNumber ] == 1) &] ]; (* :WHERE: statement*)
    interestingIntermediateResult[All, {"row_num", "total_floor_area", "TYPE3" }]
];

nHRAntecedent1a[aid_] := Module[{df, targetRowNumber, out, interestingIntermediateResult,
    where1},
   targetType3 = nHRTargetParams[aid]["targetType3"];
   df = nHRAntecedent1aSubquery1[aid];
   where1 = df[Select[(#"TYPE3" === targetType3) &] ];
   out = If[Length[where1 ] == 2 , where1[First], "Null"];
   out
   ];


End[];
EndPackage[];