(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      2889,         88]
NotebookOptionsPosition[      1999,         66]
NotebookOutlinePosition[      2434,         83]
CellTagsIndexPosition[      2391,         80]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"<<", "nnpav`"}]], "Input",
 TaggingRules->{"LastCursorPosition" -> False},
 CellChangeTimes->{
  3.767869704382765*^9, {3.8476111927812977`*^9, 3.847611194211114*^9}},
 CellLabel->"In[1]:=",ExpressionUUID->"4ad68654-788e-45f2-86d4-3916c0521231"],

Cell[BoxData["\<\"database = avm\"\>"], "Print",
 CellChangeTimes->{3.847611200094647*^9, 3.8496919963472433`*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"dab9595e-ea82-4db7-9bc7-13591783ef62"]
}, Open  ]],

Cell[BoxData[
 RowBox[{"m2TargetParams", "[", 
  TagBox[
   FrameBox["aid"],
   "Placeholder"], "]"}]], "Input",
 CellChangeTimes->{
  3.767869704382765*^9, {3.8476112171255283`*^9, 
   3.847611219937511*^9}},ExpressionUUID->"a62b8773-da16-42a7-8513-\
e9cf1ea6b3c1"],

Cell["\<\
Gryshchenko Viktoriya\[LineSeparator]Tradate Via del Pracallo 3 A
21049\
\>", "Program",
 CellChangeTimes->{{3.847622634940803*^9, 
  3.8476226751800737`*^9}},ExpressionUUID->"9045dcc0-8acc-49f0-982a-\
27403882be30"],

Cell["Via Sant'Agostino n. 4 - 22070 - Locate Varesino (Milano)", "Program",
 CellChangeTimes->{
  3.847622736412634*^9},ExpressionUUID->"fa9f8962-d72a-4f20-9932-\
9fbf5c3ed7ae"],

Cell[BoxData[
 StyleBox[
  RowBox[{"Region", "="}],
  FontWeight->"Normal"]], "DisplayFormula",
 CellChangeTimes->{{3.847622904159109*^9, 3.847622907090166*^9}},
 FontSize->13,ExpressionUUID->"161de528-5230-4907-982f-070950ccd406"]
},
WindowSize->{1266, 1041},
WindowMargins->{{Automatic, 84}, {4.5, Automatic}},
Magnification:>1. Inherited,
FrontEndVersion->"12.2 for Linux x86 (64-bit) (December 12, 2020)",
StyleDefinitions->"DefaultModified2.nb",
ExpressionUUID->"80f9b52f-243a-4f33-ad48-789366f61726"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 269, 5, 29, "Input",ExpressionUUID->"4ad68654-788e-45f2-86d4-3916c0521231"],
Cell[852, 29, 218, 4, 23, "Print",ExpressionUUID->"dab9595e-ea82-4db7-9bc7-13591783ef62"]
}, Open  ]],
Cell[1085, 36, 266, 8, 32, "Input",ExpressionUUID->"a62b8773-da16-42a7-8513-e9cf1ea6b3c1"],
Cell[1354, 46, 226, 6, 83, "Program",ExpressionUUID->"9045dcc0-8acc-49f0-982a-27403882be30"],
Cell[1583, 54, 178, 3, 49, "Program",ExpressionUUID->"fa9f8962-d72a-4f20-9932-9fbf5c3ed7ae"],
Cell[1764, 59, 231, 5, 24, "DisplayFormula",ExpressionUUID->"161de528-5230-4907-982f-070950ccd406"]
}
]
*)

